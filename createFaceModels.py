
import pickle
import os.path
import face_recognition

def generateEncodings(folderName, labelName, knownEncodings, knownNames):
    for filename in os.listdir(folderName):
        img = face_recognition.load_image_file(folderName + filename)
        boxes = face_recognition.face_locations(img, model = 'cnn')
        encodings = face_recognition.face_encodings(img, boxes)

        for encoding in encodings:
            knownEncodings.append(encoding)
            knownNames.append(labelName)

knownEncodings = []
knownNames = []

folderName = "obama/"
labelName = "obama"
generateEncodings(folderName, labelName, knownEncodings, knownNames)
       
data_encoding = {"encodings": knownEncodings, "names": knownNames}

f = open("face_encodings", "wb")
f.write(pickle.dumps(data_encoding))
f.close()
