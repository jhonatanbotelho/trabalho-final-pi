
import os
import cv2
import dlib
import os.path
from matplotlib import pyplot as plt

folder_name = "obama"
video_filename = "obama1.mp4"
shape_predictor_filename = "shape_predictor_68_face_landmarks.dat"

detector = dlib.get_frontal_face_detector()
predictor = dlib.shape_predictor(shape_predictor_filename)

if os.path.isdir(folder_name) == False:
    os.mkdir(folder_name)

def generateFolderWithFaces(folderName, videoFilename, thresholdFaces = 6, stepFrame = 10):
    detectedFacesCounter = 0
    faces = []
    frameCounter = 0

    #carrega vídeo
    videoCapture = cv2.VideoCapture(videoFilename)
    while 1:
        #contabiliza frames
        frameCounter += 1
        
        #pega o próximo frame
        success, frame = videoCapture.read()
        
        #acabou o vídeo ou detectou o limite de faces?
        if success == False or detectedFacesCounter >= thresholdFaces:
            return faces

        #"pula" alguns frames
        if frameCounter % stepFrame != 0:
            continue

        # detecta a face, o 1 indica superamostragem, pra achar faces mais facilmente
        detections = detector(frame, 1)
        for i, detection in enumerate(detections):
            
            #salvar região das faces em uma pasta
            x, y = detection.left(), detection.top()
            w, h = detection.right() - detection.left(), detection.bottom() - detection.top()
            faceCrop = frame[y:y + h, x:x + w]
            img_path = folderName + "/" + str(frameCounter) + ".jpg"
            cv2.imwrite(img_path, faceCrop)

            #essa parte é só pra adicionar retângulo ao redor da face e marcações, pra exibir bonitinho depois
            cv2.rectangle(frame, (x, y), (x + w, y + h), (255, 255, 0), 4)
            landmarks = predictor(frame, detection)
            for i in range(0, 68): 
                cv2.circle(frame, (landmarks.part(i).x , landmarks.part(i).y), 4, (0, 0, 255), -1)      
            faces.append(frame)
        
        #total de faces detectadas até o momento
        detectedFacesCounter += len(detections)
        
    videoCapture.release()

exampleFaces = generateFolderWithFaces("obama/", "obama1.mp4")

#mostra algumas faces detectadas de exemplo
fig, axes = plt.subplots(nrows = 2, ncols = 2, figsize=(16, 16))
i = 0
for y in range(0, 2):
    for x in range(0, 2):
        axes[x, y].set_title('Face ' + str(i), fontsize = 14)
        exampleFaces[i] = cv2.cvtColor(exampleFaces[i], cv2.COLOR_BGR2RGB)
        axes[x, y].imshow(exampleFaces[i])
        i += 1